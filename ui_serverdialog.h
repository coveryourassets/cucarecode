/********************************************************************************
** Form generated from reading UI file 'serverdialog.ui'
**
** Created: Tue Nov 27 21:11:17 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERDIALOG_H
#define UI_SERVERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ServerDialog
{
public:
    QPushButton *shutdown;
    QLabel *ipAddress;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLineEdit *port;
    QPushButton *reset;

    void setupUi(QDialog *ServerDialog)
    {
        if (ServerDialog->objectName().isEmpty())
            ServerDialog->setObjectName(QString::fromUtf8("ServerDialog"));
        ServerDialog->resize(400, 300);
        shutdown = new QPushButton(ServerDialog);
        shutdown->setObjectName(QString::fromUtf8("shutdown"));
        shutdown->setGeometry(QRect(290, 250, 94, 27));
        ipAddress = new QLabel(ServerDialog);
        ipAddress->setObjectName(QString::fromUtf8("ipAddress"));
        ipAddress->setGeometry(QRect(73, 20, 251, 121));
        formLayoutWidget = new QWidget(ServerDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(70, 160, 221, 80));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        port = new QLineEdit(formLayoutWidget);
        port->setObjectName(QString::fromUtf8("port"));

        formLayout->setWidget(0, QFormLayout::FieldRole, port);

        reset = new QPushButton(ServerDialog);
        reset->setObjectName(QString::fromUtf8("reset"));
        reset->setGeometry(QRect(190, 250, 94, 27));

        retranslateUi(ServerDialog);

        QMetaObject::connectSlotsByName(ServerDialog);
    } // setupUi

    void retranslateUi(QDialog *ServerDialog)
    {
        ServerDialog->setWindowTitle(QApplication::translate("ServerDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        shutdown->setText(QApplication::translate("ServerDialog", "Shut Down", 0, QApplication::UnicodeUTF8));
        ipAddress->setText(QString());
        label_2->setText(QApplication::translate("ServerDialog", "Port:", 0, QApplication::UnicodeUTF8));
        reset->setText(QApplication::translate("ServerDialog", "Reset", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ServerDialog: public Ui_ServerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERDIALOG_H
