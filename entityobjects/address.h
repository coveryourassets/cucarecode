#ifndef ADDRESS_H
#define ADDRESS_H

#include <string>
#include <cstddef>
#include <QString>

using namespace std;

class Address
{
    friend QDataStream & operator << (QDataStream & stream, Address & address);
    friend QDataStream & operator >> (QDataStream & stream, Address & address);

public:

   Address();
   Address( QString addressLineOne
          , QString addressLineTwo
          , QString city
          , qint32 province
          , QString postalCode);

    //Address(qint32 unitNumber, qint32 streetNumber, QString streetName, QString city, QString province, QString postalCode);

    //getters
    const qint32 getProvince() const;

    const QString &getAddressLineOne() const;
    const QString &getAddressLineTwo() const;
    const QString &getCity() const;
    const QString &getPostalCode() const;

    //setters
    void setProvince(qint32 province);

    void setAddressLineOne(QString &addressLineOne);
    void setAddressLineTwo(QString &addressLineTwo);
    void setCity(QString &city);
    void setPostalCode(QString &postalCode);

    //utility
    void print(QString &str);

private:

    qint32 province;

    QString addressLineOne;
    QString addressLineTwo;
    QString city;
    QString postalCode;

};

#endif // ADDRESS_H
