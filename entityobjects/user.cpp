#include "user.h"

QDataStream & operator << (QDataStream & stream, User & user){
    QString num;
    num.setNum(user.type);
    stream << num;
    num.setNum(user.userID);
    stream << num;
    stream << user.firstName;
    stream << user.lastName << user.userName << user.address;
    return stream;
}

QDataStream & operator >> (QDataStream & stream, User & user){
    QString num;
    stream >> num;
    user.type = (userType)atoi(num.toStdString().c_str());
    num.clear();
    stream >> num;
    user.userID = atoi(num.toStdString().c_str());
    stream >> user.firstName;
    stream >> user.lastName;
    stream >> user.userName;
    stream >> user.address;
    return stream;
}
User::User(){}

User::User(userType type, QString firstName, QString lastName, QString uname, Address &add, ContactInfo &info)
{
    this->type = type;
    this->userName = uname;
    this->firstName = firstName;
    this->lastName = lastName;
    this->address = add;
    this->info = info;
}


//getters
userType User::getType() const{
    return type;
}
qint32 User::getUserID() const{
    return userID;
}
const QString &User::getFirstName() const{
    return firstName;
}
const QString &User::getLastName() const{
    return lastName;
}
const QString &User::getUserName() const{
    return userName;
}

const Address &User::getAddress() {
    return address;
}

const ContactInfo &User::getContactInfo() {
    return info;
}


//setters
void User::setType(userType type){
    this->type = type;
}

void User::setUserID(qint32 userID){
    this->userID = userID;
}

void User::setFirstName(QString &firstName){
    this->firstName = firstName;
}

void User::setLastName(QString &lastName){
    this->lastName = lastName;
}

void User::setUserName(QString &userName){
    this->userName = userName;
}

void User::setAddress(Address &address){
    this->address = address;
}

void User::setContactInfo(ContactInfo &info){
    this->info = info;
}

//utility
void User::print(QString & str){
    QString num;
    str.append("User: ");
    str.append(num.setNum(userID)).append("\nFirstName: ").append(firstName);
    str.append("\nLastname: ").append(lastName);
    address.print(str);
}
