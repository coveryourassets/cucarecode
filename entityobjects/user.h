#ifndef User_H
#define User_H

#include <QString>
#include <cstddef>
#include "address.h"
#include "contactinfo.h"
using namespace std;

enum userType{Default, AdminAssistant, Physician, SysAdmin};

class User
{

    friend QDataStream & operator << (QDataStream & stream, User & address);
    friend QDataStream & operator >> (QDataStream & stream, User & address);

public:
    User();
    User(userType type, QString firstName,  QString lastName, QString uname, Address& add, ContactInfo &info);

    //getters
    userType getType() const;
    qint32 getUserID() const;
    const QString &getFirstName() const;
    const QString &getLastName() const;
    const QString &getUserName() const;
    const Address &getAddress();
    const ContactInfo &getContactInfo();

    //setters
    void setType(userType type);
    void setUserID(qint32 userID);
    void setFirstName(QString &firstName);
    void setLastName(QString &lastName);
    void setUserName(QString &userName);
    void setAddress(Address &address);
    void setContactInfo(ContactInfo &info);

    //utility
    void print(QString &);

private:
    userType type; // 0-AdministratorAssistant 1-Physician 2-SystemAdministrator
    qint32 userID;
    QString firstName;
    QString lastName;
    QString userName;
    Address address;
    ContactInfo info;
};

#endif // User_H
