#ifndef PATIENTRECROD_H
#define PATIENTRECROD_H

#include <QDate>
#include <QString>
#include <cstddef>

#include "address.h"
#include "contactinfo.h"
#include "consultation.h"

using namespace std;

class Patient
{
    friend QDataStream & operator << (QDataStream & stream, Patient & patient);
    friend QDataStream & operator >> (QDataStream & stream, Patient & patient);

public:

    Patient();

    Patient( QString healthcardno
           , QString healthinsuranceno
           , QString firstName
           , QString lastName
           , Address &address
           , ContactInfo &contactinfo);

    //getters
    qint32 getPatientID() const;
    qint32 getPrimaryPhysicianID() const;

    const QList<Consultation> &getConsultations() const;

    const QDate   &getDateOfBirth() const;
    const QString &getHealthcardNo() const;
    const QString &getHealthInsuranceNo() const;
    const QString &getFirstName() const;
    const QString &getLastName() const;
    const QString &getComments() const;

    const Address &getAddress();
    const ContactInfo &getContactInfo();

    //setters
    void setPatientID(qint32 patientID);
    void setPrimaryPhysicianID(qint32 pysID);

    void setDateOfBirth(QDate &dob);

    void setHealthcardNo(QString &healthinsuranceno);
    void setHealthInsuranceNo(QString &healthinsuranceno);
    void setFirstName(QString &firstName);
    void setLastName(QString &lastName);
    void setComments(QString &comments);

    void setAddress(Address &address);
    void setContactInfo (ContactInfo &contactinfo);

    void setConsultations (QList<Consultation> &consultation);
    void addConsultation (Consultation &consultation);

    //utility
    void print(QString &str);

private:

    qint32 patientID;
    qint32 primaryPhyID;

    QList<Consultation> consultations;

    QDate dateOfBirth;
    QString healthcardno;
    QString healthinsuranceno;
    QString firstName;
    QString lastName;
    QString comments;

    Address address;
    ContactInfo contactinfo;
    Consultation consultation;

};

#endif // PATIENTRECROD_H
