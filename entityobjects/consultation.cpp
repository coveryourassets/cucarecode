#include "consultation.h"


QDataStream & operator << (QDataStream & stream, Consultation & consult){
    stream << consult.consultID;
    stream << consult.physFirstName;
    stream << consult.physLastName;
    stream << consult.physID;
    stream << consult.patientID;
    stream << consult.date << consult.reason << consult.diagnosis;

    return stream;
}

QDataStream & operator >> (QDataStream & stream, Consultation & consult){
     stream >> consult.consultID;
     stream >> consult.physFirstName;
     stream >> consult.physLastName;
     stream >> consult.physID;
     stream >> consult.patientID;
     stream >> consult.date >> consult.reason >> consult.diagnosis;

     return stream;
}

Consultation::Consultation(){}

Consultation::Consultation( qint32 physID
                          , qint32 patientID
                          , QDateTime date
                          , QString reason)
{
    this->patientID = patientID;
    this->physID = physID;
    this->date = date;
    this->reason = reason;

}

//getters
qint32 Consultation::getConsultID() const{
     return consultID;
}


const QString &Consultation::getPhysFirstName() const{
    return physFirstName;
}
const QString &Consultation::getPhysLastName() const{
    return physLastName;
}


qint32 Consultation::getPhysID() const{
    return physID;
}

qint32 Consultation::getPatientID() const{
    return patientID;
}

const QDateTime Consultation::getDate() const{
    return date;
}

const QString &Consultation::getReason() const{
    return reason;
}

const QString &Consultation::getDiagnosis() const{
    return diagnosis;
}

QList<Followup> &Consultation::getFollowups() {
    return followups;
}


//setters
void Consultation::setConsultID(qint32 consultID){
    this->consultID = consultID;
}


void Consultation::setPhysFirstName(QString &fname){
    this->physFirstName = fname;
}

void Consultation::setPhysLastName(QString &lname){
    this->physLastName = lname;
}


void Consultation::setPhysID(qint32 physID){
    this->physID = physID;
}

void Consultation::setPatientID(qint32 patientID){
    this->patientID = patientID;
}

void Consultation::setDate(QDateTime &date){
    this->date = date;
}

void Consultation::setReason(QString &reason){
    this->reason = reason;
}

void Consultation::setDiagnosis(QString &diagnosis){
    this->diagnosis = diagnosis;
}

void Consultation::setFollowups(QList<Followup> &followups){
    this->followups = followups;
}

void Consultation::addFollowup(Followup &followup){
    followups.push_back(followup);
}

void Consultation::print(QString &str){
    QString num;
    str.append("\nConsultID: ").append(num.setNum(consultID));
    str.append("\nPatientID:").append(num.setNum(patientID));
    str.append("\nPhysId: ").append(num.setNum(physID));
    str.append("\nfname: ").append(physFirstName);
    str.append("lname: ").append(physLastName).append("\nDate: ").append(date.toString());
    str.append("\nReason: ").append(reason).append("\nDiagnosis: ").append(diagnosis);
    str.append("\n");
}
