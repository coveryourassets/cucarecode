#include "server.h"
#include "cuCareDB/custorage.h"
#include "cuCareDB/custore.h"
#include "entityobjects/address.h"
#include "serverdialog.h"
#include <QtGui>
#include <iostream>
using namespace std;

Server::Server(QObject *parent): QObject(parent)
{
    storage = new custorage();
  connect(&server, SIGNAL(newConnection()),
    this, SLOT(acceptConnection()));
  //ServerDialog dialog;
  //dialog.show();
  server.listen(QHostAddress::Any, 60000);
}

Server::~Server()
{
  server.close();
}


void Server::acceptConnection()
{
    cout << "server accept write connection"<<endl;
    //client->abort();
    client = server.nextPendingConnection();
   // connect(client, SIGNAL(disconnected()),
    //        this, SLOT(deleteLater()));
    client->waitForReadyRead(-1);

    QByteArray buf = client->readAll();
    QDataStream in(buf);
    callType type;
    in >> (qint32&)type;

    /*
    enum callType {getConsultList, saveNewConsult, updateConsult, getFupList,
      saveNewFup, updateFup, searchPatFName, searchPatLName,
      searchPatFullName, searchPatHCN, searchPatWithRR, getFURR, saveNewPat,
      updatePat, getUsr, saveNewUsr,requestCompleted, requestDenied};

      */
    switch(type){
    case getConsultList:
    {
        qDebug() << "getConsultList";
        //read in parameters
        Patient pat;
        qint32 results;
        in >> pat >> results;

        qDebug("Patient first name:");
        qDebug(pat.getFirstName().toStdString().c_str());

        //make database call
        QList<Consultation> consults;
        storage->getConsultationList(pat, consults, results);

        qDebug("consults found: %d", consults.size());

        //return info
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << consults.size();
        for (int i = 0; i < consults.size(); i ++){
            out << consults[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case saveNewConsult:
    {
        qDebug() << "saveNewConsult";

        Consultation consult;
        in >> consult;
        QString q;
        consult.print(q);
        qDebug(q.toStdString().c_str());
        storage->saveNewConsultation(consult);

        //write out everything is ok
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case updateConsult:
    {
        qDebug() << "updateConsult";

        Consultation consult;
        in >> consult;
        storage->updateConsultation(consult);

        //write out everything is ok
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case getFupList:
    {
        qDebug() << "getFupList";

        Consultation consult;
        qint32 results;
        in >> consult >> results;
        QList<Followup> followups;
        errorType t = storage->getFollowupList(consult, followups, results);
        if (t!=OK){
            qDebug("Error getting followup list");
            break;
        }

        //write the size of the list, followed by the list
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        qDebug("Followups size: %d", followups.size());
        out << followups.size();
        for (int i = 0; i < followups.size(); i ++){
            qDebug("Date: %s",followups[i].getDate().toString().toStdString().c_str());
            out << followups[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case saveNewFup:
    {
        qDebug() << "saveNewFup";

        Followup fup;
        in >> fup;
        storage->saveNewFollowup(fup);

        //write "OK!!!"
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case updateFup:
    {
        qDebug() << "updateFup";
        Followup fup;
        in >> fup;
        storage->updateFollowup(fup);

        //write "OK!!!"
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatFName:
    {
        qDebug() << "searchPatFName";

        QString firstname;
        in >> firstname;
        QList<Patient> patients;
        storage->searchPatientbyFirstName(firstname, patients);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        qDebug("Server sending # of patients: %d",patients.size() );
        out << (qint32)patients.size();
        QString s;
        //patients[0].print(s);
        //qDebug(s.toStdString().c_str());
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatLName:
    {
        qDebug() << "searchPatLName";

        QString lname;
        in >> lname;
        QList<Patient> patients;
        storage->searchPatientbyLastName(lname, patients);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)patients.size();
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatFullName:
    {
        qDebug() << "searchPatFullName";

        QString lname, fname;
        in >> lname >>fname;
        QList<Patient> patients;
        storage->searchPatientbyFullName(lname, fname, patients);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)patients.size();
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatByFup:
    {
        qDebug() << "searchPatByFup";
        Followup f;
        in >> f;
        Patient p;
        errorType t = storage ->searchPatientbyFollowup(f,p);

        //write
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        if ( t==OK){
            out << (quint32)t;
            out << p;
        }else{
            out << (quint32)t;
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatByFupStat:
    {
        qDebug() << "searchPatByFupStat";
        QList<Patient> patients;
        qint32 physID, status;
        in >> status >> physID;
        storage->searchPatientsByFollowupStatus(patients, status, physID);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)patients.size();
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatHCN:
    {
        qDebug() << "searchPatHCN";

        QString hcard;
        in >> hcard;
        QList<Patient> patients;
        storage->searchPatientbyHealthcard(hcard, patients);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)patients.size();
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case searchPatWithRR:
    {
        qDebug() << "searchPatWithRR";

        QList<Patient> patients;
        qint32 physID;
        in >> physID;
        storage->searchPatientsWithResultsReceived(patients, physID);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)patients.size();
        for (int i = 0; i < patients.size(); i++){
            out << patients[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case getFURR:
    {
        qDebug() << "getFURR";

        QList<Followup> fups;

        storage->getFollowupsWithRR(fups);

        //write them out
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)fups.size();
        for (int i = 0; i < fups.size(); i++){
            out << fups[i];
        }
        client->write(buffer);
        client->flush();
        break;
    }
    case saveNewPat:
    {
        qDebug() << "saveNewPat";

        Patient patient;
        in >> patient;
        storage->saveNewPatient(patient);

        //write "OK!!!"
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case updatePat:
    {
        qDebug() << "updatePat";

        Patient patient;
        in >> patient;
        storage->updatePatient(patient);

        //write "OK!!!"
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    case getUsr:
    {
        qDebug() << "getUsr";

        QString uname;
        in >> uname;
        cout << "User name: "<< uname.toStdString()<<endl;
        User user;
        errorType t = storage->getUser(uname, user);
        uname.clear();
        user.print(uname);
        cout << uname.toStdString();
        //write the user
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << t;
        if (t == OK){
            out << user;
        }

        client->write(buffer);
        client->flush();
        break;
    }
    case saveNewUsr:
    {
        qDebug() << "saveNewUsr";

        User user;
        in >> user;
        storage->saveNewUser(user);

        //write
        QByteArray buffer;
        QDataStream out(&buffer, QIODevice::WriteOnly);
        out << (qint32)OK;
        client->write(buffer);
        client->flush();
        break;
    }
    default:
    {
        qDebug() << "You found the easter egg!! Server: accept connect: default case.";
    }
    }



/*
    Address add;
    in >>(qint32&)type >> add;
    QString str;
    add.print(str);
    cout << "server read: "<<str.toStdString()<<endl;
    qDebug() << "did server read?";

    //////////////////////////////
    //write
    //////////////////////////////

   // QString string("bananaland");
    qint32 p = 1;
    //add.setProvince(p);
  //  add.print(str);
  //  cout << "server read 2nd printing: "<<str.toStdString()<<endl;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << type << add;
    client->write(buffer);
    client->flush();*/
    qDebug()<<"server: done writing";
}




void Server::startRead()
{
  cout << "starting server read"<<endl;

  QByteArray buf;
  buf = client->read(client->bytesAvailable());

  cout << "anything?"<< endl;
  /*
  client->write("there",5);
  client->flush();
*/

}

/**************************************
  start write
 **************************************/
void Server::startWrite(){

    cout << "starting server write"<<endl;

   // Address address(1,33,QString("writefromserver"),QString("ottawa"),QString("ontario"),QString("p6a5k8"));
/*
    QByteArray block;
    QDataStream out(&block, QIODevice::ReadWrite);
    out.setVersion(QDataStream::Qt_4_0);
    out << (quint16)0;
   // while(!readyWrite);
    out << address;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));
    cout << "server: block written"<<endl;
    client->write(block);
    cout << "server: socket written"<<endl;
    cout << "server: socket closed"<<endl;
    readyWrite = false;*/

}

