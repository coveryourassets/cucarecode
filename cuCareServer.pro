#-------------------------------------------------
#
# Project created by QtCreator 2012-11-10T18:15:44
#
#-------------------------------------------------

QT       += core network gui

TARGET = cuCareServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    cuCareDB/sqlite3.c \
    cuCareDB/custore.cpp \
    cuCareDB/custorage.cpp \
    entityobjects/user.cpp \
    entityobjects/patient.cpp \
    entityobjects/followup.cpp \
    entityobjects/contactinfo.cpp \
    entityobjects/consultation.cpp \
    entityobjects/address.cpp \
    serverdialog.cpp

HEADERS += \
    server.h \
    cuCareDB/sqlite3.h \
    cuCareDB/custore.h \
    cuCareDB/custorage.h \
    entityobjects/user.h \
    entityobjects/patient.h \
    entityobjects/followup.h \
    entityobjects/contactinfo.h \
    entityobjects/consultation.h \
    entityobjects/address.h \
    serverdialog.h

FORMS += \
    serverdialog.ui
