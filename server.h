#ifndef SERVER_H
#define SERVER_H

#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDialog>
#include "cuCareDB/custore.h"

class Server: public QObject
{
Q_OBJECT
public:
  Server(QObject *parent = 0);
  ~Server();
  //void startWrite();

public slots:
  void acceptConnection();
  void startRead();
  void startWrite();
private:
  QTcpServer server;
  QTcpSocket* client;
  cuStore* storage;
};

#endif
