#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <string>
#include "custorage.h"
#include "sqlite3.h"

custorage::custorage()
{
}

// the return value, 1 is error, 0 is AOK
errorType custorage::saveNewConsultation(Consultation & consult){
    int rc = openDatabase();
    if (rc){
        //error.append("Error: Can't open database");
        return generalError;
    }

    sqlite3_stmt* statement;

    QString id;
    QString query = "insert into consultation (patientID,";
    query.append("physID, date, reason, diagnosis) values (");
    query.append(id.setNum(consult.getPatientID())).append(",");
    id.clear();
    id.setNum(consult.getPhysID());
    query.append(id).append(",'");
    query.append(consult.getDate().toString()).append("','");
    query.append(consult.getReason()).append("','");
    query.append(consult.getDiagnosis()).append("');");

    qDebug(query.toStdString().c_str());
    //prepare the statemnt
    rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    if (rc){
        qDebug("Save consult error preparing statement: %d",rc);
        return generalError;
    }

    //execute the statment - should really check for errors
    rc = sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("Save consult error executing statement: %d",rc);
        return generalError;
    }

    sqlite3_finalize(statement);

    closeDatabase();
    return OK;

}

errorType custorage::updateConsultation(Consultation & consult){
    openDatabase();

    sqlite3_stmt* statement;

    //make the query
     QString id;
    QString query = "update consultation set reason = '";
    query.append(consult.getReason());
    query.append("',patientID = ").append(id.setNum(consult.getPatientID()));
    id.setNum(consult.getConsultID());
    query.append(",physID = ").append(id);
    query.append(", date = '").append(consult.getDate().toString());
    query.append("',diagnosis = '").append(consult.getDiagnosis()).append("'");

    query.append(" where consultID = ");

    id.setNum(consult.getConsultID());
    query.append(id).append(";");

    cout << query.toStdString()<<endl;

    //prepare the statemnt
    int rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if (rc != 0){
        qDebug("updateConsult: error preparing statement");
        closeDatabase();
        return generalError;
    }

    //execute the statment - should really check for errors
    rc = sqlite3_step(statement);

    if ((rc!=101)&&(rc!=0)){
        qDebug("updateConsult: error executing statement: %d",rc);
        sqlite3_finalize(statement);
        closeDatabase();
        return generalError;
    }
    sqlite3_finalize(statement);
    closeDatabase();
    return OK;
}

errorType custorage::getConsultationList(Patient &patient, QList<Consultation> &consults, qint32 results){
    int rc = openDatabase();
    if (rc){
        //error.append("Error: Can't open database");
        return generalError;
    }
    sqlite3_stmt* statement;
    cout<<"\npriming query\n";
    QString num;
    QString query = "select consultID, patientID, physID, lastname, firstname, date, reason, ";
    query.append("diagnosis from consultation,person where patientID = ");
    query.append(num.setNum(patient.getPatientID())).append(" and person.personID = physID");
    if(results == 1){
        query.append(" and consultID in (select consultID from followups where status = 2)");
    }
    query.append(";");
    cout << "query primed\n";
    qDebug(query.toStdString().c_str());
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    cout <<"statement prepared\n";
    getConsultsFromQuery(consults, statement);

    closeDatabase();
    return OK;
}

//followups

//given a consultation, return the list of associated followups. The
//calling function provides an empty QList that gets populated. The
//results flag is if we limit the followups to those
//whose results have been received, in which case we set it to 1. Otherwise
//it defaults to 0
errorType custorage::getFollowupList(Consultation &consult, QList<Followup> &followups, int results){
    int rc = openDatabase();
    if (rc){
        //error.append("Error: Can't open database");
        return generalError;
    }
    sqlite3_stmt* statement;
    QString num;
    QString query = "select * from followup where consultID = ";
    query.append(num.setNum(consult.getConsultID()));
    if(results == 1){
        query.append(" and status = 2");
    }
    query.append(";");

    qDebug("%s",query.toStdString().c_str());

    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    getFollowupsFromQuery(followups, statement);

    closeDatabase();
    return OK;
}

//save a new followup
errorType custorage::saveNewFollowup(Followup & followup){
    int rc = openDatabase();
    if (rc){
        //error.append("Error: Can't open database");
        return generalError;
    }

    sqlite3_stmt* statement;
  /*  rowid integer primary key,
    consultID int,
    description varchar(255),
    status tinyint,
    date varchar(20),
    results varchar(255), foreign key(consultID) references consultation(ROWID)*/

    QString query = "insert into followup (consultID, description,";
    query.append("status, date, results) values (");
    QString id;
    id.setNum(followup.getConsultID());
    query.append(id).append(",'");
    query.append(followup.getDescription()).append("',");
    id.setNum(followup.getStatus());
    query.append(id).append(",'");
    query.append(followup.getDate().toString()).append("','");
    query.append(followup.getResults()).append("');");

    qDebug("Query: %s",query.toStdString().c_str());
    //prepare the statemnt
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //execute the statment - should really check for errors
    sqlite3_step(statement);
    sqlite3_finalize(statement);

    closeDatabase();
    return OK;

}

//update a followup
errorType custorage::updateFollowup(Followup & followup){
    int rc = openDatabase();
    if (rc){
        //error.append("Error: Can't open database");
        return generalError;
    }

    sqlite3_stmt* statement;
  /*  rowid integer primary key,
    consultID int,
    description varchar(255),
    status tinyint,
    date varchar(20),
    results varchar(255), foreign key(consultID) references consultation(ROWID)*/

    QString query = "update followup set (consultID,";
    query.append("status, date, description, results) values (");
    QString id;
    id.setNum(followup.getConsultID());
    query.append(id).append(",");
    id.setNum(followup.getStatus());
    query.append(id).append(",'");
    query.append(followup.getDate().toString()).append("','");
    query.append(followup.getDescription()).append("','");
    query.append(followup.getResults()).append("')");
    query.append("where rowID = ");
    id.setNum(followup.getFollowupID());
    query.append(id).append(";");

    //prepare the statemnt
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //execute the statment - should really check for errors
    sqlite3_step(statement);
    sqlite3_finalize(statement);

    closeDatabase();
    return OK;

}

//patient records
errorType custorage::searchPatientbyFirstName(QString firstname, QList<Patient> & patients){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and firstname = '");
    query.append(firstname).append("';");
    qDebug(query.toStdString().c_str());
    int rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    if (rc){
        qDebug("error preparing statement: %d",rc);
        return generalError;
    }
    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;
}

errorType custorage::searchPatientbyLastName(QString lastname,QList<Patient> & patients){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and lastname = '");
    query.append(lastname).append("';");
    int rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    if (rc){
        qDebug("error preparing statement: %d",rc);
        return generalError;
    }

    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;
}

errorType custorage::searchPatientbyFullName(QString lastname, QString firstname, QList<Patient> & patients){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and firstname = '");
    query.append(firstname).append("' and lastname = '").append(lastname);
    query.append("';");
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;
}


errorType custorage::searchPatientbyHealthcard(QString healthcardno, QList<Patient> &patients){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and healthcardno = '");
    query.append(healthcardno ).append("';");
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;
}

errorType custorage::searchPatientsWithResultsReceived(QList<Patient> &patients, qint32 physID){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and person.personID in ");

    query.append("(select patientID from consultation where consultID in ");
    query.append("(select consultID from followup where status = 2)");
    if (physID != -1){
        query.append("and physID = ").append(physID);
    }
    query.append(");");
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;

}

errorType custorage::searchPatientsByFollowupStatus(QList<Patient> &patients, int status, int physID){
    openDatabase();
    QString num;
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID");
    if (status != -1){
        query.append("and person.personID in ");
        query.append("(select patientID from consultation where consultID in ");
        query.append("(select consultID from followup where status =").append(num.setNum(status));
        query.append(")");
    }
    if (physID != -1){
        query.append("and physID = ").append(physID);
    }
    if (status != -1){
        query.append(")");
    }
    query.append(";");
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    closeDatabase();
    return OK;
}


errorType custorage::searchPatientbyFollowup(Followup& followup, Patient &patient){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select person.personID, person.firstname, person.lastname, healthcardno, ";
    query.append("healthinsurance, line1, city, province, postalcode, ext, cellno,");
    query.append("homeno, officeno,primarycontact from person, patient, address, contact where ");
    query.append("person.personID = patientID and person.personID = address.personID");
    query.append(" and contact.personID = person.personID and healthcardno in ");


    query.append("(select healthcardno from consultation ");
    query.append("where consultID in (select consultID from followup where followupID = ");
    query.append(followup.getFollowupID()).append("));");
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    QList<Patient> patients;
    //load the QList with the results of the query
    getPatientsFromQuery(patients, statement);
    patient = patients[0];
    closeDatabase();
    return OK;
}


errorType custorage::getFollowupsWithRR(QList<Followup> &followups){
    openDatabase();
    sqlite3_stmt* statement;
    QString query = "select * from followup where status = 2";
    int rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    if (rc!=0){
        qDebug("getFWRR: statement did not prepare");
        closeDatabase();
        return generalError;
    }
    //load the QList with the results of the query
    getFollowupsFromQuery(followups, statement);
    closeDatabase();
    return OK;
}


errorType custorage::saveNewPatient(Patient & patient){
    qDebug("saving new patient");

    int rc = openDatabase();
    if (rc){
        qDebug("Error: Can't open database");
        return generalError;
    }
    qDebug("database open");

    //begin a transaction
    sqlite3_exec(db,"BEGIN TRANSACTION;", NULL, NULL, NULL);
    qint32 pid = insertPerson(patient.getFirstName(), patient.getLastName());

    if (pid == -1){
        qDebug("new patient: person did not insert");
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    sqlite3_stmt* statement;
    QString query = "insert into patient (patientID, healthcardno, ";
    query.append("healthinsurance, dateofbirth, comments) values (");
    QString id;
    id.setNum(pid);
    qDebug("ID value %d",pid);
    query.append(id).append(",'");
    query.append(patient.getHealthcardNo()).append("','");
    query.append(patient.getHealthInsuranceNo()).append("','");
    query.append(patient.getDateOfBirth().toString()).append("','");
    query.append(patient.getComments()).append("');");

    rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("statement did not prepare: %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }
    qDebug("statement prepared");

    //execute the statment - should really check for errors
    rc = sqlite3_step(statement);
    if ((rc != 0)&&(rc != 101)){
        qDebug("Error with step: %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }
    qDebug("statement executed");
    sqlite3_finalize(statement);

    //address
    const Address *add = &patient.getAddress();
    rc = (int)insertAddress(pid, add);
    if ((rc != 0)&&(rc != 101)){
        qDebug("insertPatient: could not insert address %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    //contactinfo
    const ContactInfo *info = &patient.getContactInfo();
    rc = (int)insertContactInfo(pid, info);
    if ((rc != 0)&&(rc != 101)){
        qDebug("insertPatient: could not insert contactinfo %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    sqlite3_exec(db,"END TRANSACTION;", NULL, NULL, NULL);
    closeDatabase();
    qDebug("Successfully inserted patient");
    return OK;
}

errorType custorage::updatePatient(Patient & patient){
    qDebug("saving new patient");

    int rc = openDatabase();
    if (rc){
        qDebug("Error: Can't open database");
        return generalError;
    }
    qDebug("database open");

    //begin a transaction
    sqlite3_exec(db,"BEGIN TRANSACTION;", NULL, NULL, NULL);
    qint32 pid = updatePerson(patient.getPatientID(), patient.getFirstName(), patient.getLastName());

    if (pid == -1){
        qDebug("new patient: person did not update");
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    sqlite3_stmt* statement;
    QString num;
    QString query = "update patient set healthcardno = '";
    query.append(patient.getHealthcardNo()).append("', healthinsurance = '");
    query.append(patient.getHealthInsuranceNo()).append("', dateofbirth = '");
    query.append(patient.getDateOfBirth().toString()).append("', comments = '");
    query.append(patient.getComments()).append("' where patientID = ");
    query.append(num.setNum(patient.getPatientID())).append(";");

    qDebug("Query: %s",query.toStdString().c_str());

    rc = sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("statement did not prepare: %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }
    qDebug("statement prepared");

    //execute the statment - should really check for errors
    rc = sqlite3_step(statement);
    if ((rc != 0)&&(rc != 101)){
        qDebug("Error with step: %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }
    qDebug("statement executed");
    sqlite3_finalize(statement);

    //address
    const Address *add = &patient.getAddress();
    rc = (int)updateAddress(pid, add);
    if ((rc != 0)&&(rc != 101)){
        qDebug("updatePatient: could not update address %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    //contactinfo
    const ContactInfo *info = &patient.getContactInfo();
    rc = (int)updateContactInfo(pid, info);
    if ((rc != 0)&&(rc != 101)){
        qDebug("updatePatient: could not update contactinfo %d",rc);
        sqlite3_exec(db,"ROLLBACK;", NULL, NULL, NULL);
        closeDatabase();
        return generalError;
    }

    sqlite3_exec(db,"END TRANSACTION;", NULL, NULL, NULL);
    closeDatabase();
    qDebug("Successfully updated patient");

    return OK;
}


//user records
errorType custorage::getUser(QString userName, User &user){
    openDatabase();
    sqlite3_stmt* statement;
    cout << "User name: "<<userName.toStdString() << endl;
    QString query("select * from user where username = '");
    query.append(userName).append("';");
    cout << query.toStdString()<<endl;
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    //load the QList with the results of the query
    errorType t = getUserFromQuery(statement, user);
    closeDatabase();
    return t;
}

errorType custorage::saveNewUser(User &user){
    qDebug("saving new user");

    int rc = openDatabase();
    if (rc){
        qDebug("Error: Can't open database");
        return generalError;
    }
    qDebug("database open");

    sqlite3_stmt* statement;
    /*
    userType type; // 0-AdministratorAssistant 1-Physician 2-SystemAdministrator
    qint32 userID;
    QString firstName;
    QString lastName;
    QString userName;
    Address address;

    QString query = "insert into user (firstname,";
    query.append("lastname, username, type, unitnumber, streetnumber,");
    query.append("streetname, city, province, postalcode) values ('");
    QString id;
    query.append(user.getFirstName()).append("','");
    query.append(user.getLastName()).append("','");
    query.append(user.getUserName()).append("',");
    id.setNum(user.getType());
    query.append(id).append(",");


    //address
    const Address *add = &user.getAddress();
    id.setNum(add->getUnitNumber());
    query.append(id).append(",");
    id.setNum(add->getStreetNumber());
    query.append(id).append(",'");
    query.append(add->getStreetName()).append("','");
    query.append(add->getCity()).append("','");
    query.append(add->getProvince()).append("','");
    query.append(add->getPostalCode()).append("');");

    //prepare the statemnt
    cout << query.toStdString()<<endl;
    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    qDebug("statement prepared");


    //execute the statment - should really check for errors
    sqlite3_step(statement);
    qDebug("statement executed");

    sqlite3_finalize(statement);
    qDebug("statement finalized");


    closeDatabase();*/
    return OK;
}

errorType custorage::updateUser(User &user){

    return OK;
}


//other
errorType custorage::createDatabase(){
    openDatabase();
    string query = "create table patient(rowid integer primary key, ";
    query.append("healthcardno varchar(20), healthinsurance varchar(20), ");
    query.append("lastname varchar(255), firstname varchar(255),");
    query.append("unitnumber int, streetnumber int, streetname varchar(50), ");
    query.append("city varchar(50), province varchar(50), postalcode varchar(7));");

    sqlite3_stmt* statement;
    sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL);
    sqlite3_step(statement);

    query.clear();

    return OK;

}


//testing
errorType custorage::testPrepared(){
    openDatabase();
    sqlite3_stmt* statement;
    sqlite3_prepare_v2(db, "select * from consultation",
                                          -1, &statement, NULL);
    sqlite3_step(statement);
    char *s = (char*)sqlite3_column_text(statement, 0);
    cout << "rowid = "<<s<<endl;
    s = (char*)sqlite3_column_text(statement, 3);
    cout <<"physid = "<<s<<endl;
    sqlite3_finalize(statement);
    closeDatabase();
    return OK;
}

//private

/* Database should be open
 * Insert a new person into the database and return their uniqueid
 */
qint32 custorage::insertPerson(QString fname, QString lname){
    sqlite3_stmt* statement;
    QString query = "insert into person (firstname, lastname) values ('";
    query.append(fname).append("','").append(lname).append("');");

    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    int rc = sqlite3_step(statement);
    sqlite3_finalize(statement);
    if ((rc == 101)||(rc == 0)){
        return (qint32)sqlite3_last_insert_rowid(db);
    }
    return -1;

}

qint32 custorage::updatePerson(qint32 personID, QString fname, QString lname){
    sqlite3_stmt* statement;
    QString num;
    QString query = "update person set firstname = '";
    query.append(fname).append("',lastname = '");
    query.append(lname).append("' where personID = ");
    query.append(num.setNum(personID)).append(";");
    qDebug("Query: %s",query.toStdString().c_str());

    sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);

    int rc = sqlite3_step(statement);
    sqlite3_finalize(statement);
    if ((rc == 101)||(rc == 0)){
        return personID;
    }
    return -1;
}

qint32 custorage::insertAddress(qint32 id, const Address *add){
    sqlite3_stmt* statement;
    QString i;

    i.setNum(id);
    QString query = "insert into address (personID, line1, line2, ";
    query.append("city, province, postalcode) values (");
    query.append(i).append(",'");
    query.append(add->getAddressLineOne()).append("','");
    query.append(add->getAddressLineTwo()).append("','");
    query.append(add->getCity()).append("',");
    query.append(i.setNum(add->getProvince())).append(",'");
    query.append(add->getPostalCode()).append("');");

    //prepare the statemnt
    cout << query.toStdString()<<endl;
    quint32 rc = (qint32)sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("Address: statement did not prepare: %d",rc);
        return rc;
    }
    qDebug("statement prepared");


    //execute the statment - should really check for errors
    rc = (qint32)sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("Error with step: %d",rc);
        return rc;
    }
    qDebug("statement executed");

    sqlite3_finalize(statement);
    qDebug("statement finalized");
    return 0;
}

qint32 custorage::updateAddress(qint32 id, const Address *add){
    sqlite3_stmt* statement;
    QString i;

    QString query = "update address set line1 = '";
    query.append(add->getAddressLineOne()).append("', line2 = '");
    query.append(add->getAddressLineTwo()).append("',city = '");
    query.append(add->getCity()).append("',province = ");
    query.append(i.setNum(add->getProvince())).append(", postalcode ='");
    query.append(add->getPostalCode()).append("' where personID = ");
    query.append(i.setNum(id)).append(";");

    //prepare the statemnt
    cout << query.toStdString()<<endl;
    quint32 rc = (qint32)sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("update Address: statement did not prepare: %d",rc);
        return rc;
    }
    qDebug("statement prepared");


    //execute the statment - should really check for errors
    rc = (qint32)sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("Error with step: %d",rc);
        return rc;
    }
    qDebug("statement executed");

    sqlite3_finalize(statement);
    qDebug("statement finalized");
    return 0;
}

qint32 custorage::insertContactInfo(qint32 id, const ContactInfo *info){
    sqlite3_stmt* statement;
    QString i;

    i.setNum(id);
    QString query = "insert into contact (personID, ext, cellno, homeno,";
    query.append("officeno, primarycontact) values (");
    query.append(i).append(",'");
    query.append(info->getExt()).append("','");
    query.append(info->getCellNo()).append("','");
    query.append(info->getHomeNo()).append("','");
    query.append(info->getOfficeNo()).append("','");
    query.append(info->getPrimaryNo()).append("');");

    //prepare the statemnt
    qDebug("Query: %s",query.toStdString().c_str());
    quint32 rc = (qint32)sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("Address: statement did not prepare: %d",rc);
        return rc;
    }
    qDebug("statement prepared");


    //execute the statment - should really check for errors
    rc = (qint32)sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("Error with step: %d",rc);
        return rc;
    }
    qDebug("statement executed");

    sqlite3_finalize(statement);
    qDebug("statement finalized");
    return 0;
}

qint32 custorage::updateContactInfo(qint32 id, const ContactInfo *info){
    sqlite3_stmt* statement;
    QString i;

    i.setNum(id);
    QString query = "update contact set ext = '";
    query.append(info->getExt()).append("', cellno = '");
    query.append(info->getCellNo()).append("', homeno = '");
    query.append(info->getHomeNo()).append("', officeno = '");
    query.append(info->getOfficeNo()).append("', primarycontact = '");
    query.append(info->getPrimaryNo()).append("' where personID = ");
    query.append(i).append(";");

    //prepare the statemnt
    qDebug("Query: %s",query.toStdString().c_str());
    quint32 rc = (qint32)sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if(rc != 0){
        qDebug("update contact: statement did not prepare: %d",rc);
        return rc;
    }
    qDebug("statement prepared");


    //execute the statment - should really check for errors
    rc = (qint32)sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("Error with step: %d",rc);
        return rc;
    }
    qDebug("statement executed");

    sqlite3_finalize(statement);
    qDebug("statement finalized");
    return 0;
}


errorType custorage::getUserFromQuery(sqlite3_stmt* statement, User &user){
    Address add;
    ContactInfo info;
    QString store;
    char *str;
    int result = sqlite3_step(statement);
    cout << "set first name"<< endl;
    //this should only return one value
    if (result == SQLITE_ROW){
        cout << "in the row ***********************************"<<endl;
        str = (char*)sqlite3_column_text(statement, 0);
        user.setUserID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 1);
        user.setType((userType)atoi(str));
        str = (char*)sqlite3_column_text(statement, 2);
        store.clear();
        store.append(str);
        user.setUserName(store);

        QString fname, lname;
        errorType t = getPersonFromID(user.getUserID(), fname, lname);
        if (t==generalError){
            qDebug("error getting person from id");
            return t;
        }
        user.setFirstName(fname);
        user.setLastName(lname);


        //address
        //t=getAddress(user.getUserID(), add);
        if (t==generalError){
            qDebug("error getting address in getuserfromquery");
            return t;
        }

        user.setAddress(add);

        //t=getContactInfor(user.getUserID(), info);
        if (t==generalError){
            qDebug("error getting contactinfo in getuserfromquery");
            return t;
        }

        user.setContactInfo(info);

        result = sqlite3_step(statement);
        return OK;
    }
    return recordNotFound;

}

errorType custorage::getPersonFromID(qint32 id, QString &fname, QString &lname){
    sqlite3_stmt* statement;
    QString num;
    QString query = "select firstname, lastname from person where personID = ";
    query.append(num.setNum(id)).append(";");
    qDebug("%s",query.toStdString().c_str());
    quint32 rc = (qint32)sqlite3_prepare_v2(db, query.toStdString().c_str(), -1, &statement, NULL);
    if (rc){
        qDebug("error preparing statement: getPersonFromID");
        return generalError;
    }
    rc = sqlite3_step(statement);
    if ((rc!=0)&&(rc!=101)){
        qDebug("error stepping statement: getPersonFromID: %d",rc);
        //return generalError;
    }
    char* str;
    while (rc == SQLITE_ROW){
        cout << "in the row ***********************************"<<endl;
        str = (char*)sqlite3_column_text(statement, 0);
        lname.clear();
        lname.append(str);
        str = (char*)sqlite3_column_text(statement, 1);
        fname.clear();
        fname.append(str);
        rc = sqlite3_step(statement);
    }

}


errorType custorage::getPatientsFromQuery(QList<Patient> &patients, sqlite3_stmt* statement){
    Address add;
    ContactInfo info;
    Patient* pat;
    QString store;
    char *str;
    int result = sqlite3_step(statement);
    qDebug("executed patient search");
    while (result == SQLITE_ROW){
        qDebug("found patients");
        pat = new Patient();
        str = (char*)sqlite3_column_text(statement, 0);
        pat->setPatientID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 1);
        store.clear();
        store.append(str);
        pat->setFirstName(store);
        str = (char*)sqlite3_column_text(statement, 2);
        store.clear();
        store.append(str);
        pat->setLastName(store);

        str = (char*)sqlite3_column_text(statement, 3);
        store.clear();
        store.append(str);
        pat->setHealthcardNo(store);
        str = (char*)sqlite3_column_text(statement, 4);
        store.clear();
        store.append(str);
        pat->setHealthInsuranceNo(store);

        getAddress(5, add, statement);
        //getAddress(pat->getPatientID(), add);

        pat->setAddress(add);

        getContactInfo(9, info, statement);

        pat->setContactInfo(info);

        patients.push_back(*pat);

        result = sqlite3_step(statement);

    }
    return OK;

}

errorType custorage::getConsultsFromQuery(QList<Consultation> &consults, sqlite3_stmt* statement){
    char * str;
    Consultation* consult;
    QDateTime date;
    QString store;
    int result = sqlite3_step(statement);
    while (result == SQLITE_ROW){
        consult = new Consultation();
        str = (char*)sqlite3_column_text(statement, 0);
        consult->setConsultID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 1);
        consult->setPatientID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 2);
        consult->setPhysID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 3);
        store.clear();
        store.append(str);
        consult->setPhysFirstName(store);
        str = (char*)sqlite3_column_text(statement, 4);
        store.clear();
        store.append(str);
        consult->setPhysLastName(store);
        str = (char*)sqlite3_column_text(statement, 5);
        parseDate(str, date);
        consult->setDate(date);
        str = (char*)sqlite3_column_text(statement, 6);
        store.clear();
        store.append(str);
        consult->setReason(store);
        str = (char*)sqlite3_column_text(statement, 7);
        store.clear();
        store.append(str);
        consult->setDiagnosis(store);

        consults.push_back(*consult);
        result = sqlite3_step(statement);

    }
    return OK;

}

errorType custorage::getFollowupsFromQuery(QList<Followup> &followups, sqlite3_stmt* statement){
    char * str;
    Followup* followup;
    QDate date;
    QString store;
    int result = sqlite3_step(statement);
    while (result == SQLITE_ROW){
        followup = new Followup();
     /*
    qint32 followupID;
    qint32 consultID;
    QString description;
    Status status;
    QDateTime duedate;
    QString results;
    */
        str = (char*)sqlite3_column_text(statement, 0);
        followup->setFollowupID(atoi(str));
        str = (char*)sqlite3_column_text(statement, 1);
        followup->setConsultID(atoi(str));

        str = (char*)sqlite3_column_text(statement, 2);
        followup->setStatus(getStatusFromInt(atoi(str)));
        str = (char*)sqlite3_column_text(statement, 3);
        QString q(str);
        date = date.fromString(q);
        followup->setDate(date);
        str = (char*)sqlite3_column_text(statement, 4);
        store.clear();
        store.append(str);
        followup->setDescription(store);
        str = (char*)sqlite3_column_text(statement, 5);
        store.clear();
        store.append(str);
        followup->setResults(store);

        followups.push_back(*followup);
        result = sqlite3_step(statement);
        qDebug("Descr: %s", followup->getDescription().toStdString().c_str());
    }
    return OK;
}

Status custorage::getStatusFromInt(int stat){
    switch(stat){
    case 0: return pending;
    case 1: return overdue;
    case 2: return results_received;
    case 3: return complete;
    }
    return pending;
}

 // start refers to what column in the sql database the address starts at
errorType custorage::getAddress(qint32 start, Address & add, sqlite3_stmt* statement){

    char * str;
    QString store;

    str = (char*)sqlite3_column_text(statement, start);
    store.clear();
    store.append(str);
    add.setAddressLineOne(store);

 /*   str = (char*)sqlite3_column_text(statement, start +1);
    store.clear();
    store.append(str);
    add.setAddressLineTwo(store);*/

    str = (char*)sqlite3_column_text(statement, start +1);
    store.clear();
    store.append(str);
    add.setCity(store);

    str = (char*)sqlite3_column_text(statement, start +2);
    add.setProvince(atoi(str));

    str = (char*)sqlite3_column_text(statement, start +3);
    store.clear();
    store.append(str);
    add.setPostalCode(store);

    return OK;
}

errorType custorage::getContactInfo(qint32 start, ContactInfo & info, sqlite3_stmt *statement){

    char * str;
    QString store;

    str = (char*)sqlite3_column_text(statement, start);
    store.clear();
    store.append(str);
    info.setExt(store);

    str = (char*)sqlite3_column_text(statement, start + 1);
    store.clear();
    store.append(str);
    info.setCellNo(store);

    str = (char*)sqlite3_column_text(statement, start +2);
    store.clear();
    store.append(str);
    info.setHomeNo(store);

    str = (char*)sqlite3_column_text(statement, start +3);
    store.clear();
    store.append(str);
    info.setOfficeNo(store);

    str = (char*)sqlite3_column_text(statement, start +4);
    store.clear();
    store.append(str);
    qDebug("getting contact info");
    qDebug(str);
    info.setPrimaryNo(store);

    return OK;
}

void custorage::parseDate(const char * str, QDateTime &date){
    QString q(str);
    date = date.fromString(q);
}



int custorage::openDatabase(){
    int rc = sqlite3_open("cucare.db", &db);

    if(rc)
    {
      //
      std::cout << "Can't open database\n";
    }else{
      std::cout << "Open database successfully\n";
    }
    return rc;
}


void custorage::closeDatabase(){
    if(db)
    {
      sqlite3_close(db);
    }
}

/*
QString custorage::convertInt(int number)
{
   QString ss(number);//add number to the stream
   return ss;//return a string with the contents of the stream
}

QString custorage::convertLong(long number)
{
   QString ss(number);//add number to the stream
   return ss;//return a string with the contents of the stream
}*/
