#ifndef CUSTORE_H
#define CUSTORE_H

#include "../entityobjects/consultation.h"
#include "../entityobjects/patient.h"
#include "../entityobjects/user.h"
#include "../entityobjects/address.h"

enum callType {getConsultList, saveNewConsult, updateConsult, getFupList,
               saveNewFup, updateFup, searchPatFName, searchPatLName,
               searchPatFullName, searchPatHCN, searchPatWithRR, searchPatByFupStat, searchPatByFup, getFURR, saveNewPat,
               updatePat, getUsr, saveNewUsr, updateUsr, requestCompleted, requestDenied};

enum errorType {OK, generalError, recordNotFound};

class cuStore
{
public:

    //consultation records

    //given a patient, return the list of associated consultations. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the consultations to those with followups
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType virtual getConsultationList(Patient &patient, QList<Consultation> &consults, int results = 0)=0;

    //save a new consultation.
    errorType virtual saveNewConsultation(Consultation &)=0;

    //update a consultation
    errorType virtual updateConsultation(Consultation &)=0;

    //followups

    //given a consultation, return the list of associated followups. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the followups to those
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType virtual getFollowupList(Consultation &consults, QList<Followup> &followups, int results = 0)=0;

    //save a new followup
    errorType virtual saveNewFollowup(Followup &followup)=0;

    //update a followup
    errorType virtual updateFollowup(Followup & followup)=0;

    //patient records

    //given a firstname, lastname, or both, return the list of associated Patients. The
    //calling function provides an empty QList that gets populated
    errorType virtual searchPatientbyFirstName(QString firstname, QList<Patient> &patients)=0;
    errorType virtual searchPatientbyLastName(QString lastname, QList<Patient> &patients)=0;
    errorType virtual searchPatientbyFullName(QString lastname, QString firstname, QList<Patient> &patients)=0;
    errorType virtual searchPatientbyFollowup(Followup& followup, Patient &patient)=0;

    //given a healthcardno, return the list (there should only be one) of patients. The
    //calling function provides an empty QList that gets populated
    errorType virtual searchPatientbyHealthcard(QString healthcardno, QList<Patient> &patients)=0;

    //return patients who have recieved results, filtered by physician (optional)
    errorType virtual searchPatientsWithResultsReceived(QList<Patient> &patients, int physID = -1)=0;

    errorType virtual searchPatientsByFollowupStatus(QList<Patient> &patients, int status, int physID)=0;

    errorType virtual getFollowupsWithRR(QList<Followup> &followups)=0;

    //not part of the requirements, but used in testing
    errorType virtual saveNewPatient(Patient & patient)=0;
    errorType virtual updatePatient(Patient & patient)=0;


    //user records

    //Given a userName, return the user. The calling function provides
    //an empty user class to populate
    errorType virtual getUser(QString userName, User &user)=0;

    errorType virtual updateUser(User &user)=0;

    //for testing purposes
    errorType virtual saveNewUser(User &user)=0;

    //other
    errorType virtual createDatabase()=0;

private:


};

#endif // CUSTORE_H

