#ifndef CUSTORAGE_H
#define CUSTORAGE_H

#include "../entityobjects/consultation.h"
#include "../entityobjects/patient.h"
#include "../entityobjects/user.h"
#include "../entityobjects/address.h"
#include "sqlite3.h"
#include <sstream>
#include "custore.h"

/*
  A simple interface for this. No SQL allowed in the header.
  SQL is completely within the implementation file
  */
class custorage: public cuStore
{
public:
    custorage();

    //consultation records

    //given a patient, return the list of associated consultations. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the consultations to those with followups
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType getConsultationList(Patient &patient, QList<Consultation> &consults, int results = 0);

    //save a new consultation.
    errorType saveNewConsultation(Consultation &);

    //update a consultation
    errorType updateConsultation(Consultation &);

    //followups

    //given a consultation, return the list of associated followups. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the followups to those
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType getFollowupList(Consultation &consults, QList<Followup> &followups, int results = 0);

    //save a new followup
    errorType saveNewFollowup(Followup &followup);

    //update a followup
    errorType updateFollowup(Followup & followup);

    //patient records

    //given a firstname, lastname, or both, return the list of associated Patients. The
    //calling function provides an empty QList that gets populated
    errorType searchPatientbyFirstName(QString firstname, QList<Patient> &patients);
    errorType searchPatientbyLastName(QString lastname, QList<Patient> &patients);
    errorType searchPatientbyFullName(QString lastname, QString firstname, QList<Patient> &patients);
    errorType searchPatientbyFollowup(Followup& followup, Patient &patient);


    //given a healthcardno, return the list (there should only be one) of patients. The
    //calling function provides an empty QList that gets populated
    errorType searchPatientbyHealthcard(QString healthcardno, QList<Patient> &patients);

    //return patients who have recieved results, filtered by physician (optional)
    errorType searchPatientsWithResultsReceived(QList<Patient> &patients, int physID = -1);

    errorType searchPatientsByFollowupStatus(QList<Patient> &patients, int status, int physID);

    errorType getFollowupsWithRR(QList<Followup> &followups);


    //not part of the requirements, but used in testing
    errorType saveNewPatient(Patient & patient);
    errorType updatePatient(Patient & patient);


    //user records

    //Given a userName, return the user. The calling function provides
    //an empty user class to populate
    errorType getUser(QString userName, User &user);

    errorType updateUser(User &user);

    //for testing purposes
    errorType saveNewUser(User &user);

    //other
    errorType createDatabase();

    //testing
    errorType testPrepared();

private:

    //utility
    qint32 insertPerson(QString fname, QString lname);
    qint32 insertAddress(qint32 id, const Address *add);
    qint32 insertContactInfo(qint32 id, const ContactInfo *info);
    qint32 updatePerson(qint32 personID, QString fname, QString lname);
    qint32 updateAddress(qint32 id, const Address *add);
    qint32 updateContactInfo(qint32 id, const ContactInfo *info);

    errorType getUserFromQuery(sqlite3_stmt* statement, User &user);
    Status getStatusFromInt(int stat);
    errorType getAddress(qint32 start, Address &add, sqlite3_stmt *statement);
    errorType getContactInfo(qint32 start, ContactInfo & info,sqlite3_stmt *statement);
    errorType getPatientsFromQuery(QList<Patient> &patients, sqlite3_stmt* statement);
    errorType getConsultsFromQuery(QList<Consultation> &consults, sqlite3_stmt* statement);
    errorType getFollowupsFromQuery(QList<Followup> &followups, sqlite3_stmt* statement);
    errorType getPersonFromID(qint32 id, QString &fname, QString &lname);
    void parseDate(const char * str, QDateTime &date);

    //the int returns an error value if applicable
    int openDatabase();
    void closeDatabase();
    QString convertInt(int);
    QString convertLong(long number);
    sqlite3* db;
};

#endif // CUSTORAGE_H
